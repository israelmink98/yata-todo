import * as mongoose from 'mongoose';

export const TaskSchema = new mongoose.Schema({
    titre : {type : String, required : true },
    description : {type : String ,default : ''},
    active : {type : Boolean, default: true}

});

export interface Task extends mongoose.Document{ 
         id: string;
         titre: string;
         description: string;
         active: boolean; 
}
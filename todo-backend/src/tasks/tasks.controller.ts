import { Controller, Get, Post, Body, Patch, Param, Delete,} from '@nestjs/common';
import { TasksService } from './tasks.service';


@Controller('tasks')
export class TasksController {
  constructor(private readonly tasksService: TasksService) {}

  @Post()
 async createTask(
   @Body('titre') taskTitre: string,
   @Body ('description') taskDesc: string
   ) {
     const generatedId = await this.tasksService.createTask(
       taskTitre,
       taskDesc,
     );
    return {id: generatedId};
  }

  @Get()
  getAllTasks() {
    return this.tasksService.getAllTasks();
  }

  @Get(':id')
  getOneTask(@Param('id') taskId: string) {
    return this.tasksService.getOneTask(taskId);
  }

  @Patch(':id')
  updateTask(
    @Param('id') taskId : string,
    @Body('titre') taskTitre : string,
    @Body('description') taskDesc : string,
    @Body('active') active: boolean
    ) {
      this.tasksService.updateTask(taskId, taskTitre, taskDesc,active);
    return null;
  }

  @Delete(':id')
  removeTask(@Param('id') taskId: string) {
    this.tasksService.removeTask(taskId);
    return null;
  }
}

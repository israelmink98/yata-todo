import { TaskSchema } from './task.model';
import { MongooseModule } from '@nestjs/mongoose';
import { Module } from '@nestjs/common';
import { TasksService } from './tasks.service';
import { TasksController } from './tasks.controller';




@Module({
  imports: [
    MongooseModule.forFeature([{name: 'Task', schema: TaskSchema}]),
    ],
  controllers: [TasksController],
  providers: [TasksService]
})
export class TasksModule {}

import { Injectable, NotFoundException,} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Task } from './task.model';


@Injectable()
export class TasksService {


  constructor(@InjectModel('Task') private readonly taskModel: Model<Task>){

  }

  async createTask(titre: string, desc: string) {
    const newTask = new this.taskModel({
       titre : titre,
       description : desc,
       active : true
      });
    const result = await newTask.save();
  
    return result.id as string ;
  }

 async getAllTasks() {
    const tasks = await this.taskModel.find().exec();
    return tasks.map((task) => ({
      id : task.id,
      titre : task.titre,
      description : task.description,
      active : task.active
    }));
  }

 async getOneTask(taskId: string) {
   const task = await this.findTask(taskId);
    return {
      id: task.id,
      titre : task.titre,
      description: task.description,
      active:task.active
    };
  }

async  updateTask(
    taskId: string,
    titre: string,
    desc: string,
    active: boolean 
    
    ) {
      const updateTask =await this.findTask(taskId);

      if(titre) updateTask.titre = titre;
      if(desc) updateTask.description = desc;
      if(active) updateTask.active = active;

    updateTask.save();
  }

  async removeTask(taskId: string) {
    const result = await this.taskModel.deleteOne({_id: taskId}).exec();
    if (result.n === 0){
      throw new NotFoundException('Ne peut pas trouver la tache que vous voulez.');
    }

    }
    private async findTask(id: string): Promise<Task>{
      let task;
      try {
        task = await this.taskModel.findById(id).exec();
      } catch (error) {
        throw new NotFoundException('Ne peut pas trouver la tache.');
      }
      if(!task){
        throw new NotFoundException('ne peut pas trouver la tache.')
      }
      return task;
    }

  }



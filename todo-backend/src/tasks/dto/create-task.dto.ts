
import { MaxLength, IsNotEmpty, IsEmail, IsString } from 'class-validator';

export class CreateTaskDto {
    @IsString()
  @MaxLength(30)
  @IsNotEmpty()
  readonly titre: string;

  @IsString()
  @MaxLength(50)
  readonly description: string;
}

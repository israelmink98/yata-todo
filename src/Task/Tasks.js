import React, {Component} from 'react';
import {Input,Button,ListGroup, ListGroupItem, Badge } from 'reactstrap';
import RLDD from 'react-list-drag-and-drop/lib/RLDD';
import './task.css';
import checkIcon from '../icons/icon-check.svg';
import crossIcon from '../icons/icon-cross.svg';
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";

class Tasks extends Component {
  constructor(){
    super();
    this.state ={
      userInput:'',
      taches:[],
      titre:'',
      description:'',
      index: 0,
      filtre:[],
    }
    this.completeTask = this.completeTask.bind(this);
    this.create = this.create.bind(this);
    this.update = this.update.bind(this);
    this.delete = this.delete.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  create(e) {
    // add entity - POST
    e.preventDefault();
    // creates entity
    fetch("http://localhost:5000/tasks", {
      method: "POST",
      headers: {
        "Content-Type": 'application/json'
      },
      body: JSON.stringify({
        titre: this.state.userInput,
        description: this.state.description,
      })
    })
    .then(response => response.json())
    .then(response => {
      console.log(response)
    })
    .catch(err => {
      console.log(err);
    });
  }

  addTask=(event)=>{
   // event.preventDefault();
    const data = {
      id: this.state.index,
      titre: this.state.userInput,
      description: '',
      active:true,
    }
    this.setState({
      taches:[...this.state.taches, data],
      index: this.state.index + 1,
      userInput:'',
      filtre:[...this.state.filtre,data]
    
    },()=> console.log(this.state.taches));
  }

  componentDidMount(){
    fetch("http://localhost:5000/tasks")
    .then(response => response.json())
    .then((data)=> {
      console.log("les données recus de la bd",data);
      this.setState({taches : data, filtre: data}) 
    })
    .catch((error) => {console.log("error", error)});
  }

  update(e) {
    // update entity - PUT
    e.preventDefault();
    // this will update entries with PUT
    fetch(`http://localhost:5000/tasks/${this.state.id}`, {
        method: "PATCH",
      headers: {
        "Content-Type": 'application/json'
      },
        body: JSON.stringify({
            titre: this.state.titre,
            description: this.state.description
        })
        })
        .then(response => response.json())
        .then(response => { console.log(response);
        })
        .catch(err => { console.log(err); });
  }
  
  delete(e) {
    // delete entity - DELETE
    e.preventDefault();
    // deletes entities
    fetch(`http://localhost:5000/tasks/${this.state.id}`, {
      method: "DELETE"
    })
    .then(response => response.json())
    .then(response => {
      console.log(response);
    })
    .catch(err => {
      console.log(err);
    });
  }
  handleChange(changeObject) {
    this.setState(changeObject)
  }

  handleChange(changeObject) {
    this.setState(changeObject)
  }

  onChange = (event) => {
    
    this.setState({ 
      userInput : event.target.value
    }, () => console.log(this.state.userInput));
    
  }
  
  completeTask=(item)=>{
    console.log("tache completée");
    this.state.taches.map((elem) => {
      if(item.id === elem.id){
        elem.active = !elem.active
        console.log(elem.active);

      }
      
    })

    this.forceUpdate();
    
  }
 
  handleRLDDChange = (newTache) => {
    this.setState({
      taches: newTache
    })
  }

 renderTasks(){
   return this.state.taches.map((tache) =>{
     return (
       <div  align="left" className="list-group-item form-check align-items-center" key={tache} >
        <button type="button" className="btn btn-floating">x</button> {tache}
       </div>
       
     );
   }

   );
 }
 completetask = () => {
    const taches = this.state.filtre.filter(t => t.active === false);
    this.setState({taches});
  }
  activetask = () => {
    const taches = this.state.filtre.filter(t => t.active !== false);
    this.setState({taches});
  }
  allTask = ()=> {

 this.setState({taches : this.state.filtre});

  }
  deleteAllTask =() => {
   const taches = this.state.filtre.filter(t => t.active !== false);
    this.setState({filtre :  taches});
  }

 render(){
  
   return(
      <div className="d-block p-2">
    
          <form onSubmit={this.create} >
          <span className="item-menu nom-tache">
          <button type="button" className="btn-default btn-normal fictif" id="boutton"></button>
              <Input 
                className="form-control "
                placeholder="Créer une nouvelle tache"
                type="text" 
                autoFocus= {true}
                value={this.state.userInput} 
                onChange={this.onChange.bind(this)}
                onFocus={this.allTask}
                
              />
          </span>
          </form>
        <div className="bg-white rounded">

          <div className="list-group" style={{maxHeight: '70vh', overflow: 'auto'}}>
              {
        
                <DragDropContext onDragEnd={(props) => {}} >

                  <Droppable droppableId="droppable-1" >
                    {(provided) => (

                          <div className="task" {...provided.droppableProps} ref={provided.innerRef} >

                          {this.state.taches.map((tache , i) => {
                       return (<Draggable 
                              key={tache.id} 
                              draggableId={tache.id} 
                              index={i} >

                              {(provided) => (
            
                                tache.active ? 
                                  <div 
                                  {...provided.draggableProps}
                                   ref={provided.innerRef} 
                                   {...provided.dragHandleProps}
                                   className="list-group-item item">
                                    <span className = "item-menu">
                                      <button type="button" className="btn-default btn-normal" onClick={() => this.completeTask(tache)} id="boutton">
                                    </button>
                                    {tache.titre} 
                                    <button type="button" className="btn-default btn-cross" >
                                      <img className="icons" src={crossIcon} alt='icone de suppression'></img>
                                    </button> </span>
                                  </div>
                                  : 
                                  <div 
                                  {...provided.draggableProps}
                                   ref={provided.innerRef} 
                                   {...provided.dragHandleProps} 
                                   className="list-group-item item">
                                    <span style={{textDecoration : 'line-through', color: 'gray'}} className = "item-menu">
                                      <button type="button" className="btn-default btn-circle" onClick={() => this.completeTask(tache)} id="boutton">
                                        <img className="icons" src={checkIcon}></img>
                                    </button>
                                    {tache.titre}
                                    <button type="button" className="btn-default btn-cross" >
                                      <img className="icons" src={crossIcon}></img>
                                    </button>
                                    </span>
                                  </div>
                  
                              )} 
                              </Draggable>
                              
                          );
                              
                          })
                     }                          
                          {provided.placeholder}
                    </div>
                    )}
                  </Droppable>
              </DragDropContext> 
              }         
          </div>
        </div>    
        <div className="footer bg-white rounded">
            <span className="infotask" ><Badge pill>{this.state.taches.filter(t => t.active !== false).length}</Badge>        tâche(s) restante(s)</span>
            <button className="btn btm" onClick={this.allTask}>Tout</button>
            <button className="btn btm" onClick={this.activetask}>Active</button>
            <button className="btn btm" onClick={this.completetask} >Completé</button>
            <button className="btn btm del" 
            onClick={() => {this.allTask(); this.deleteAllTask(); }}>
            Effacer les taches terminées</button>
          </div>
          
      </div>
    );
  
 } 
} 

export default Tasks;
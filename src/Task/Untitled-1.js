import logo from './logo.svg';
import React from 'react';
import './App.css';
import Task from './Task/Task';
import {
  Jumbotron, 
  Button,
  Card,
  CardBody,
  CardGroup,
  Col,
  Container,
  Form,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row,
  FormGroup, Label,FormText
} from "reactstrap";
import desktopImg from './images/bg-desktop-light.jpg'; 

function App() {
  return (
    
    <div className="App">
      <div className="page" /*styles = {{backgroundImage: `url(${desktopImg})`}}*/ >
        <div className="contenu container">
          <task />
          
              <div className="title">
                <h1 className="align-left " >TO DO</h1>
              </div>
                
                <div className="justify-content-center ">
                  <div className="col-6 justify-content-center " ><Input placeholder="Créer une nouvelle tache" type="text" /></div>
                  <div className="taches">
                    <Task taskName ="aller a UBA"/>
                  </div>
                  
              
                  <div className="list-group">
            {this.renderTasks()}
          </div>  





              </div>
            </div>

        </div>
    </div>
  );
}

export default App;

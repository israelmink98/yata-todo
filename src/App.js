import logo from './logo.svg';
import React from 'react';
import './App.css';
import Task from './Task/Tasks';
import RLDD from 'react-list-drag-and-drop/lib/RLDD';
import {
  Jumbotron, 
  Button,
  Card,
  CardBody,
  CardGroup,
  Col,
  Container,
  Form,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row,
  FormGroup, Label,FormText
} from "reactstrap";
import desktopImg from './images/bg-desktop-light.jpg';

function App() {
  return (
    
    <div className="App">
    <div className="bgd"></div>
      <div className="container container-small contenu ">
      <h1 className="display-6"  style={{ color: 'White' }} align="left" >T O  D O</h1>
          <Task />
     
      </div>
                <p className="footext" style={{ color: 'Gray' }}>Faites glisser et déposer pour réorganiser la liste</p>

    </div>
  );
}

export default App;
